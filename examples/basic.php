<?php

$array = [
    'name' => 'Walter',
    'age' => 23,
    'birth' => 1983
];


$builder = new Array2Text($array, 'Dados pessoais:');

$builder->add('name')->before('Nome')->noBreak(/* Default separator: space */)
        ->add('age')->after('anos.')
        ->add('birth')->before('Idade:')->break(2)
        ->footer('-------------')
        ->build();
